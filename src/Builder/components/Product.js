// @flow
import * as React from "react";
import { Card } from "react-bulma-components";

import type { ComponentOptionSchema, RenderedComponentProps } from "../types";

export const OPTIONS: ComponentOptionSchema = {
  title: {
    label: "title",
    type: "string",
  },
  description: {
    label: "description",
    type: "string",
  },
  price: {
    label: "price",
    type: "number",
  },
  image: {
    label: "image source",
    type: "string",
  },
};

export const ID = "product";

function Product({ options }: RenderedComponentProps): React.Node {
  const { title, description, image, price } = options;
  return (
    <div className="product">
      <Card>
        <Card.Image src={image} />
        <Card.Header>
          <Card.Header.Title>{title || "Missing title"}</Card.Header.Title>
          <span className="price">{price}</span>
        </Card.Header>
        <Card.Content>
          <p>{description}</p>
        </Card.Content>
      </Card>
    </div>
  );
}

export const Component = Product;
