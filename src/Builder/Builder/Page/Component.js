// @flow
import * as React from "react";

import type { Component as ComponentType } from "../../types";

import { Panel, Modal, Button, Form } from "react-bulma-components";

type Props = {|
  component: ComponentType,
  onChange: (ComponentType) => void,
  onRemove: (ComponentType) => void,
|};

export default function BuilderPageComponent({
  onChange,
  component,
  onRemove,
}: Props): React.Node {
  // Question 5
  // Ajouter la fonctionnalité d'édition du contenu d'un composant :
  // - Permettre d'ouvrir la modal lors du click sur le bouton edit
  // - Afficher un formulaire permettant d'éditer les options du composant
  // - Enregistrer les changements lorsque l'utilisateur cliques sur "Enregistrer"
  // Note: les changements ne doivent pas prendre effet tant que l'utilisateur n'a pas enregistrer

  const { componentId, options } = component;

  return (
    <>
      <Panel color="warning">
        <Panel.Header>
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div>{componentId}</div>
            <div>
              <Button size="small" style={{ marginRight: 5 }}>
                <span class="icon">
                  <i class="fas fa-edit"></i>
                </span>
              </Button>
              <Button
                onClick={() => onRemove(component)}
                size="small"
                color="danger"
              >
                <span class="icon">
                  <i class="fas fa-times"></i>
                </span>
              </Button>
            </div>
          </div>
        </Panel.Header>

        {Object.keys(options).map((optionName) => (
          <Panel.Block>
            <p
              style={{
                textOverflow: "ellipsis",
                whiteSpace: "nowrap",
                overflow: "hidden",
              }}
            >
              <b>{optionName}</b>: {options[optionName] || "Empty"}
            </p>
          </Panel.Block>
        ))}
      </Panel>

      <Modal>
        <Modal.Card>
          <Modal.Card.Header>
            <Modal.Card.Title>
              Options du composant {componentId}
            </Modal.Card.Title>
          </Modal.Card.Header>

          <Modal.Card.Body>
            {/* Formulaire d'édition des options du composant */}
          </Modal.Card.Body>

          <Modal.Card.Footer hasAddons style={{ justifyContent: "flex-end" }}>
            <Button>Annuler</Button>
            <Button color="success">Enregistrer</Button>
          </Modal.Card.Footer>
        </Modal.Card>
      </Modal>
    </>
  );
}
